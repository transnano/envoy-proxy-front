package main

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func handler(c *gin.Context) {
	c.String(http.StatusOK, "Hello, World. %s(%s)", os.Getenv("SERVICE_NAME"), c.Param("id"))
}

func main() {
	r := gin.Default()
	r.GET("/service/:id", handler)
	r.GET("/trace/:id", handler)
	r.Run() // listen and serve on 0.0.0.0:8080
}
